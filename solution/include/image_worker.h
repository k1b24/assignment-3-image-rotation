#ifndef IMAGE_TRANSFORMER_IMAGE_WORKER_H
#define IMAGE_TRANSFORMER_IMAGE_WORKER_H

#include "image_struct.h"
#include "stdio.h"

size_t count_padding(struct image const* img);

void free_image(struct image* img);

#endif //IMAGE_TRANSFORMER_IMAGE_WORKER_H
