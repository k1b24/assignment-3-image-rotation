#ifndef IMAGE_STRUCT
#define IMAGE_STRUCT

#include <inttypes.h>

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

#endif
