#ifndef BMP_ROTATOR
#define BMP_ROTATOR
#include "image_struct.h"

struct rotation_result rotate( struct image source );

enum rotate_status  {
    ROTATE_OK = 0,
    ROTATE_ERROR_WHILE_MALLOC = 1
};

struct rotation_result {
    struct image img;
    enum rotate_status status;
};

#endif //BMP_ROTATOR
