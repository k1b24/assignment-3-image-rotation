#include "image_struct.h"
#include "stdio.h"

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE = 1,
    READ_INVALID_BITS = 2,
    READ_INVALID_HEADER = 3,
    READ_FILE_IS_NULL = 4,
    READ_PIXELS_ERROR = 5,
    MEMORY_ALLOCATION_WHILE_READ_ERROR = 6,
    FSEEK_WHILE_READ_ERROR = 7
};

enum read_status from_bmp( FILE* in, struct image* img );

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR = 1,
    MEMORY_ALLOCATION_WHILE_WRITE_ERROR = 2
};

enum write_status to_bmp( FILE* out, struct image const* img );
