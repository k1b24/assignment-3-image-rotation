#include "image_struct.h"
#include "malloc.h"

#define PIXEL_SIZE 4
#define PIXEL_STRUCT_SIZE 3

uint8_t count_padding(struct image const* img) {
    return (PIXEL_SIZE - ((PIXEL_STRUCT_SIZE * img->width) % PIXEL_SIZE));
}

void free_image(struct image* img) {
    if (img->data != NULL) {
        free(img->data);
        img->data = NULL;
    }
    free(img);
}
