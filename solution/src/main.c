#include "bmp_file_worker.h"
#include "bmp_rotator.h"
#include "image_worker.h"
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    if (argc != 3) {
        fprintf(stderr, "The number of arguments must be 3 \n");
        return 1;
    }
    int return_code = 0;
    FILE* file_from = fopen(argv[1], "rb");
    if (file_from == NULL) {
        return 2;
    }
    struct image* image = malloc(sizeof(struct image));
    if (image == NULL) {
        return 2;
    }
    enum read_status status = from_bmp(file_from, image);
    if (fclose(file_from)) {
        fprintf(stderr, "An error occurred while closing the input file");
        return_code = 2;
    }
    if (status == READ_OK) {
        FILE* file_to = fopen(argv[2], "wb");
        if (file_to == NULL) {
            free_image(image);
            return 2;
        }
        struct rotation_result out = rotate(*image);
        if (out.status == ROTATE_ERROR_WHILE_MALLOC) {
            return 2;
        } else {
            to_bmp(file_to, &(out.img));
            free(out.img.data);
            if (fclose(file_to)) {
                fprintf(stderr, "An error occurred while closing the output file");
                return_code = 2;
            }
        }

    }
    free_image(image);
    return return_code;
}
