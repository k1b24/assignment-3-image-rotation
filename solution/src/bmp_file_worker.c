#include "bmp_header.h"
#include "bmp_file_worker.h"
#include "image_struct.h"
#include "image_worker.h"
#include "stdio.h"
#include "stdlib.h"


#define NUM_OF_BITS 24
#define PIXEL_SIZE 4
#define PIXEL_STRUCT_SIZE 3

#define FILE_TYPE 19778
#define RESERVED 0
#define HEADERS_SIZE 54
#define INFO_HEADERS_SIZE 40
#define PLANES 1
#define BIT_COUNT 24
#define COMPRESSION 0
#define DEFAULT_X_PIXELS_PER_METER 2834
#define DEFAULT_Y_PIXELS_PER_METER 2834



enum read_status from_bmp( FILE* in, struct image* img ) {
    if (in == NULL) {
        return READ_FILE_IS_NULL;
    }
    struct bmp_header* header = malloc(sizeof(struct bmp_header));
    if (header == NULL) {
        return MEMORY_ALLOCATION_WHILE_READ_ERROR;
    }
    uint64_t status = fread(header, sizeof(struct bmp_header), 1, in);
    if (!status) {
        free(header);
        return READ_INVALID_HEADER;
    }
    if (header->biBitCount != NUM_OF_BITS) {
        free(header);
        return READ_INVALID_BITS;
    }
    img->width = header->biWidth;
    img->height = header->biHeight;
    img->data = malloc(img->width * img->height * PIXEL_STRUCT_SIZE);
    if (img->data == NULL) {
        free(header);
        return MEMORY_ALLOCATION_WHILE_READ_ERROR;
    }
    size_t padding = count_padding(img);
    size_t counter = 0;
    if (fseek(in, header->bOffBits, SEEK_SET)) {
        free(header);
        free(img->data);
        img->data = NULL;
        return FSEEK_WHILE_READ_ERROR;
    }
    free(header);
    for (uint64_t i = 0; i < img->height; i++) {
        for (uint64_t j = 0; j < img->width; j++) {
            status = fread(img->data + counter, sizeof(struct pixel), 1, in);
            if (!status) {
                free(img->data);
                img->data = NULL;
                return READ_PIXELS_ERROR;
            }
            counter++;
        }
        if (fseek(in, (int8_t) padding, SEEK_CUR)) {
            free(img->data);
            img->data = NULL;
            return FSEEK_WHILE_READ_ERROR;
        }
    }
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img) {
    struct bmp_header* header = malloc(sizeof(struct bmp_header));
    if (header == NULL) {
        return MEMORY_ALLOCATION_WHILE_WRITE_ERROR;
    }

    size_t padding = count_padding(img);

    header->bfType = FILE_TYPE;
    header->bfileSize = img->width * img->height * PIXEL_STRUCT_SIZE + HEADERS_SIZE + img->height * padding;
    header->bfReserved = RESERVED;
    header->bOffBits = HEADERS_SIZE;
    header->biSize = INFO_HEADERS_SIZE;
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->biPlanes = PLANES;
    header->biBitCount = BIT_COUNT;
    header->biCompression = COMPRESSION;
    header->biSizeImage = img->width * img->height; //count
    header->biXPelsPerMeter = DEFAULT_X_PIXELS_PER_METER;
    header->biYPelsPerMeter = DEFAULT_Y_PIXELS_PER_METER;
    header->biClrUsed = 0;
    header->biClrImportant = 0;

    uint64_t status = fwrite(header, sizeof(struct bmp_header), 1, out);
    free(header);
    if (!status) {
        return WRITE_ERROR;
    }
    int counter = 0;
    size_t zero_byte = 0;
    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            status = fwrite(img->data + counter, sizeof(struct pixel), 1, out);
            if (!status) {
                return WRITE_ERROR;
            }
            counter++;
        }

        status = fwrite(&zero_byte, (int8_t) padding, 1, out);
        if (!status) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
