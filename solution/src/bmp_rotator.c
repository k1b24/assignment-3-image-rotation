#include "bmp_rotator.h"
#include "image_struct.h"
#include "stdlib.h"
#include <malloc.h>


#define PIXEL_STRUCT_SIZE 3

struct rotation_result rotate(struct image const source) {
    struct image out;
    out.height = source.width;
    out.width = source.height;
    out.data = malloc(PIXEL_STRUCT_SIZE * out.width * out.height);
    if (out.data == NULL) {
        struct rotation_result result = {out, ROTATE_ERROR_WHILE_MALLOC};
        return result;
    }
    for (uint64_t i = 0; i < source.height; i++) {
        for (uint64_t j = 0; j < source.width; j++) {
            out.data[out.width - i + j * source.height - 1] = source.data[source.width * i + j];
        }
    }
    struct rotation_result result = {out, ROTATE_OK};
    return result;
}
